# cavalcade-2018

Created for "13brane's holidazed cavalcade of code shenanigans: the flavors of FizzBuzz" (2018)

Relevant rants on 13brane.net:

- <https://13brane.net/rants/holidazed-cavalcade-2018-day-0/>
- <https://13brane.net/rants/holidazed-cavalcade-2018-day-1-8/>
- <https://13brane.net/rants/holidazed-cavalcade-2018-day-9-16/>
- <https://13brane.net/rants/holidazed-cavalcade-2018-day-17-24/>

Index:

*(Procedural approaches)*
- **day 1**: The no BS, competent junior dev approach.
- **day 2**: CompSci 101: Structured Programming (Good student version).
- **day 3**: CompSci 101: (Un)Structured Programming (Bad student version).
- **day 4**: Advanced student.
- **day 5**: "Advanceder", DRY student.
- **day 6**: `else` considered harmful.
- **day 7**: Performance optimization.
- **day 8**: Trying too hard.

*(Functional approaches)*
- **day 9**: CompSci 232 Programming Paradigms, sophomore student.
- **day 10**: Functional Programming, finalist undergrad.
- **day 11**: Functional Programming, master's student.
- **day 12**: Functional Programming, PhD student.
- **day 13**: Functional Programming, PhD advisor.
- **day 14**: Functional Programming, Tenured Professor.
- **day 15**: l33t h4x0r.
- **day 16**: uber-l33t h4x0r.

*(Object Oriented and other approaches)*
- **day 17**: Object Oriented, competent, pragmatic junior dev.
- **day 18**: Object Oriented, hip startup version.
- **day 19**: Object Oriented, established internet company version.
- **day 20**: Object Oriented, fully OOssified.
- **day 21**: Generalized FizzBuzz DSL.
- **day 22**: SmarTazz: All-in-one, XML configurable, abstract enterprise rules
  engine for on-prem FizzBuzzBazzing.
- **day 23**: @carlosdavidepto showing off.
- **day 24**: @carlosdavidepto being pragmatic.
