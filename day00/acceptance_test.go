package day00_test

import (
	"fmt"
	"os"
	"testing"

	// we'll add the implementations here...
	"gitlab.com/carlosdavidepto/cavalcade-2018/day01"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day02"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day03"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day04"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day05"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day06"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day07"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day08"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day09"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day10"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day11"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day12"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day13"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day14"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day15"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day16"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day17"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day18"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day19"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day20"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day21"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day22"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day23"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day24"
)

func TestAllImplementations(t *testing.T) {
	// set config path for day22, CWD is day00 as per package `testing` doc
	_ = os.Setenv(day22.CONFIG_PATH_ENV_VAR, "../day22/config.xml")

	implementations := []func(int) string{
		// ... and here.
		day01.FizzBuzz,
		day02.FizzBuzz,
		day03.FizzBuzz,
		day04.FizzBuzz,
		day05.FizzBuzz,
		day06.FizzBuzz,
		day07.FizzBuzz,
		day08.FizzBuzz,
		day09.FizzBuzz,
		day10.FizzBuzz,
		day11.FizzBuzz,
		day12.FizzBuzz,
		day13.FizzBuzz,
		day14.FizzBuzz,
		day15.FizzBuzz,
		day16.FizzBuzz,
		day17.FizzBuzz,
		day18.FizzBuzz,
		day19.FizzBuzz,
		day20.FizzBuzz,
		day21.FizzBuzz,
		day22.MakeXMLRulesApplicator(), // xD hilarious.
		day23.FizzBuzz,
		day24.FizzBuzz,
	}

	for _, FizzBuzzImpl := range implementations {
		testMultiplesOf3and5OutputFizzBuzz(t, FizzBuzzImpl)
		testMultiplesOf3OutputFizz(t, FizzBuzzImpl)
		testMultiplesOf5OutputBuzz(t, FizzBuzzImpl)
		testEveryOtherValueOutputsInputConvertedToString(t, FizzBuzzImpl)
	}
}

func testMultiplesOf3and5OutputFizzBuzz(t *testing.T, FizzBuzz func(int) string) {
	for i := 15; i <= 100; i = i + 15 {
		// 3 and 5 are the only prime factors of 15
		// therefore, all multiples of 3 and 5 are also multiples of 15
		if FizzBuzz(i) != "fizzbuzz" {
			t.Errorf("Expected %v, got %v in FizzBuzz(%v)", "fizzbuzz", FizzBuzz(i), i)
		}
	}
}

func testMultiplesOf3OutputFizz(t *testing.T, FizzBuzz func(int) string) {
	for i := 3; i <= 100; i = i + 3 {
		if i%5 != 0 {
			// ignore multiples of 3 which are also multiples of 5 here
			// they are tested in the first case
			if FizzBuzz(i) != "fizz" {
				t.Errorf("Expected %v, got %v in FizzBuzz(%v)", "fizz", FizzBuzz(i), i)
			}
		}
	}
}

func testMultiplesOf5OutputBuzz(t *testing.T, FizzBuzz func(int) string) {
	for i := 5; i <= 100; i = i + 5 {
		if i%3 != 0 {
			// ignore multiples of 5 which are also multiples of 3 here
			// they are tested in the first case
			if FizzBuzz(i) != "buzz" {
				t.Errorf("Expected %v, got %v in FizzBuzz(%v)", "buzz", FizzBuzz(i), i)
			}
		}
	}
}

func testEveryOtherValueOutputsInputConvertedToString(t *testing.T, FizzBuzz func(int) string) {
	for i := 1; i <= 100; i++ {
		if i%3 != 0 && i%5 != 0 {
			// ignore multiples of 5 and multiples of 3 here
			// they are tested in the other cases
			if FizzBuzz(i) != fmt.Sprint(i) {
				t.Errorf("Expected %v, got %v in FizzBuzz(%v)", fmt.Sprint(i), FizzBuzz(i), i)
			}
		}
	}
}
