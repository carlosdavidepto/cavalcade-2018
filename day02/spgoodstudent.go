package day02

import "fmt"

func FizzBuzz(n int) string {
	var divBy3 bool = n%3 == 0
	var divBy5 bool = n%5 == 0
	var out string = ""

	if divBy3 {
		out = out + "fizz"
	}

	if divBy5 {
		out = out + "buzz"
	}

	if len(out) == 0 {
		out = fmt.Sprint(n)
	}

	return out
}
