package day03

import "fmt"

var divBy3 bool
var by5 bool
var out string
var flag bool

func FizzBuzz(n int) string {
	// clean variable
	divBy3 = false
	by5 = false
	out = ""
	flag = false

	divBy3 = n%3 == 0
	by5 = n%5 == 0

	if divBy3 {
		out = out + "fizz"
		flag = true
	}

	if by5 {
		out = out + "buzz"
		flag = true
	}

	if !flag {
		out = fmt.Sprint(n)
	}

	return out
}
