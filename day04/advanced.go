package day04

import "fmt"

func FizzBuzz(n int) string {
	var out string = ""

	if divBy3(n) {
		out = out + "fizz"
	}

	if divBy5(n) {
		out = out + "buzz"
	}

	if len(out) == 0 {
		out = fmt.Sprint(n)
	}

	return out
}

func divBy3(n int) bool {
	return n%3 == 0
}

func divBy5(n int) bool {
	return n%5 == 0
}
