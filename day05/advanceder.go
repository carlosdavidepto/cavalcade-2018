package day05

import "fmt"

func FizzBuzz(n int) string {
	var out string = ""

	if divBy(n, 3) {
		out = out + "fizz"
	}

	if divBy(n, 5) {
		out = out + "buzz"
	}

	if len(out) == 0 {
		out = fmt.Sprint(n)
	}

	return out
}

func divBy(n, d int) bool {
	return n%d == 0
}
