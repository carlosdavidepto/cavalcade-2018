package day07

import "fmt"

func FizzBuzz(n int) string {
	if n%3 == 0 {

		if n%5 == 0 {
			return "fizzbuzz"
		}

		return "fizz"
	}

	if n%5 == 0 {
		return "buzz"
	}

	return fmt.Sprint(n)
}
