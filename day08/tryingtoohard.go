package day08

// import the "fmt" package, we'll need fmt.Sprint to convert integers to
// strings.
import "fmt"

// FizzBuzz takes a positive integer and returns a string. If the passed in
// integer is divisible by 3, the output is "fizz". If it is divisible by 5,
// the output is "buzz". If it is divisible by 3 and 5, the output is
// "FizzBuzz". Otherwise, the output is the number itself, converted to its
// decimal string representation.
func FizzBuzz(n int) string {

	// all numbers need to be tested for divisibility by both factors.
	// therefore, we optimize the program by performing these divisions only
	// once
	divisibleby3 := n%3 == 0
	divisibleby5 := n%5 == 0

	if divisibleby3 {
		// number is divisible by 3

		// each number must be tested for both factors
		if divisibleby5 {
			// number is divisible by 3 and 5.
			return "fizzbuzz"
		}

		// number is not divisible by 5, only by 3
		return "fizz"
	}

	// number is not divisible by 3 from this point on

	if divisibleby5 {
		// number is divisible by 5
		return "buzz"
	}

	// number does not have 3 or 5 as prime factors
	return fmt.Sprint(n)
}
