package day09

import "fmt"

type IntString struct {
	i int
	s string
}

func FizzBuzz(i int) string {
	return stringOf(buzzOf(fizzOf(IntString{i, ""})))
}

func fizzOf(a IntString) IntString {
	if a.i%3 == 0 {
		return IntString{a.i, a.s + "fizz"}
	} else {
		return a
	}
}

func buzzOf(a IntString) IntString {
	if a.i%5 == 0 {
		return IntString{a.i, a.s + "buzz"}
	} else {
		return a
	}
}

func stringOf(a IntString) string {
	if len(a.s) == 0 {
		return fmt.Sprint(a.i)
	} else {
		return a.s
	}
}
