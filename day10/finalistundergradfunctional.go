package day10

import "fmt"

// tuple, (int, string)
type IntString struct {
	i int
	s string
}

// FizzBuzz :: int -> string
func FizzBuzz(i int) string {
	return stringOf(buzzOf(fizzOf(IntString{i, ""})))
}

// fizzOf :: (int, string) -> (int, string)
func fizzOf(x IntString) IntString {
	return xof(divisibleBy(3), "fizz")(x)
}

// buzzOf :: (int, string) -> (int, string)
func buzzOf(x IntString) IntString {
	return xof(divisibleBy(5), "buzz")(x)
}

// xof :: (int -> bool) -> string -> ((int, string) -> (int, string))
func xof(check func(int) bool, rule string) func(IntString) IntString {
	return func(a IntString) IntString {
		if check(a.i) {
			return IntString{a.i, a.s + rule}
		} else {
			return a
		}
	}
}

// stringOf :: (int, string) -> string
func stringOf(a IntString) string {
	if len(a.s) == 0 {
		return fmt.Sprint(a.i)
	} else {
		return a.s
	}
}

// divisibleBy :: int -> (int -> bool)
func divisibleBy(n int) func(int) bool {
	return func(i int) bool {
		return i%n == 0
	}
}
