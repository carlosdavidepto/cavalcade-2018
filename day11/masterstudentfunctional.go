package day11

import "fmt"

// Why aren't we doing this in Haskell, again?

// (int, string) tuple
type IntString struct {
	i int
	s string
}

// simulate deriving Show for IntString
func (x IntString) String() string {
	if len(x.s) == 0 {
		return fmt.Sprint(x.i)
	} else {
		return x.s
	}
}

// FizzBuzz :: int -> string
func FizzBuzz(n int) string {

	// looks rather stupid without `let`

	// divisibleBy :: int -> (int -> bool)
	divisibleBy := func(n int) func(int) bool {
		return func(i int) bool {
			return i%n == 0
		}
	}

	// xof :: (int -> bool) -> string -> ((int, string) -> (int, string))
	xof := func(check func(int) bool, rule string) func(IntString) IntString {
		return func(x IntString) IntString {
			if check(x.i) {
				return IntString{x.i, x.s + rule}
			} else {
				return x
			}
		}
	}

	// currying? Would if I could.

	// fizzOf :: (int, string) -> (int, string)
	fizzOf := xof(divisibleBy(3), "fizz")

	// buzzOf :: (int, string) -> (int, string)
	buzzOf := xof(divisibleBy(5), "buzz")

	return buzzOf(fizzOf(IntString{n, ""})).String()
}
