package day12

import "fmt"

// This is just silly. We could have used Rust.

// Let's pretend we have monads, destructuring, pattern matching
// and data constructors.

// data FB = AFizz a | ABuzz a | AFizzBuzz a | APlain a
type FB interface {
	Plain() int
	String() string
}

type APlain struct {
	p int
}

func (p APlain) Plain() int {
	return p.p
}
func (p APlain) String() string {
	return fmt.Sprint(p.p)
}

type AFizz struct {
	APlain
}

func (f AFizz) String() string {
	return "fizz"
}

type ABuzz struct {
	APlain
}

func (b ABuzz) String() string {
	return "buzz"
}

type AFizzBuzz struct {
	APlain
}

func (f AFizzBuzz) String() string {
	return "fizzbuzz"
}

func FizzBuzz(i int) string {
	return buzz(fizz(APlain{i})).String()
}

// really, I'm going to ask my advisor why he's an idiot.

func fizz(x FB) FB {
	mod3 := x.Plain()%3 == 0

	if mod3 {
		switch x.(type) {
		case APlain:
			return AFizz{APlain{x.Plain()}}
		case ABuzz:
			return AFizzBuzz{APlain{x.Plain()}}
		default:
			return x
		}
	} else {
		return x
	}
}

func buzz(x FB) FB {
	mod5 := x.Plain()%5 == 0

	if mod5 {
		switch x.(type) {
		case APlain:
			return ABuzz{APlain{x.Plain()}}
		case AFizz:
			return AFizzBuzz{APlain{x.Plain()}}
		default:
			return x
		}
	} else {
		return x
	}
}

// oh god, kill me now.
