package day13

import "fmt"

// my master's and phd students are dense. It's just one of those batches. If
// they got off imgur I'm sure their output would improve, but cat pictures
// are a good use of student loans in 2018... The point of the exercise was
// to understand exactly how much effort is required to translate functional
// features to a non functional language. Translating Haskell to Haskell is
// not a good use of grants. Nimrods.

type (
	// data FB = Fizz FB | Buzz FB | Just a
	FB interface {
		Value() int
	}

	Just struct {
		i int
	}

	Buzz struct {
		fb FB
	}

	Fizz struct {
		fb FB
	}
)

// make constructors part of data class
func (j Just) Value() int { return j.i }
func (b Buzz) Value() int { return b.fb.Value() }
func (f Fizz) Value() int { return f.fb.Value() }

// buzz :: int -> FB
func buzz(n int) FB {
	if n%5 == 0 {
		return Buzz{Just{n}}
	} else {
		return Just{n}
	}
}

// fizz :: FB -> FB
func fizz(fb FB) FB {
	if fb.Value()%3 == 0 {
		return Fizz{fb}
	} else {
		return fb
	}
}

// show :: FB -> string -> string
func show(fb FB, prev string) string {
	switch v := fb.(type) {
	case Just:
		if len(prev) != 0 {
			return prev
		} else {
			return fmt.Sprint(v.i)
		}
	case Fizz:
		return show(v.fb, prev+"fizz")
	case Buzz:
		return show(v.fb, prev+"buzz")
	default:
		return "" // never happens, only makes compiler happy
	}
}

func FizzBuzz(n int) string {
	return show(fizz(buzz(n)), "")
}
