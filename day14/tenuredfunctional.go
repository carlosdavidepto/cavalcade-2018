package day14

import "fmt"

// Note to Dr. Ethelbert:
// I am not sure the students are the problem.

func FizzBuzz(n int) string {
	return IfThenElse(
		Cond(n%3 == 0),
		IfThenElse(
			Cond(n%5 == 0),
			Value("fizzbuzz"),
			Value("fizz"),
		),
		IfThenElse(
			Cond(n%5 == 0),
			Value("buzz"),
			ToString(n),
		),
	).Solve()
}

// Haskell is for wimps. For a true hair-shirt experience, you should
// implement a (crude) lambda calculus interpreter instead.
type Calc struct {
	f func() string
}

func (c Calc) Solve() string {
	return c.f()
}

func True(x, _ Calc) Calc {
	return x
}

func False(_, y Calc) Calc {
	return y
}

func IfThenElse(c func(Calc, Calc) Calc, t, f Calc) Calc {
	return c(t, f)
}

// Go->Lambda and Lambda->Go transformations

func Cond(b bool) func(Calc, Calc) Calc {
	return map[bool]func(Calc, Calc) Calc{true: True, false: False}[b]
}

func Value(s string) Calc {
	return Calc{func() string {
		return s
	}}
}

func ToString(n int) Calc {
	return Calc{func() string {
		return fmt.Sprint(n)
	}}
}

// Oh, and we need to talk about grant allocations...
