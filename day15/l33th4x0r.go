package day15

import "fmt"

func FizzBuzz(n int) string {
	x := [15]string{
		"fizzbuzz", "", "", "fizz", "",
		"buzz", "fizz", "", "", "fizz",
		"buzz", "", "fizz", "", "",
	}[n%15]

	if x == "" {
		return fmt.Sprint(n)
	} else {
		return x
	}
}
