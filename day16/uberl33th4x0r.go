package day16

import "fmt"

func fb(_ int) string { return "fizzbuzz" }
func f(_ int) string  { return "fizz" }
func b(_ int) string  { return "buzz" }
func s(n int) string  { return fmt.Sprint(n) }

func FizzBuzz(n int) string {
	return [15]func(int) string{
		fb, s, s, f, s,
		b, f, s, s, f,
		b, s, f, s, s,
	}[n%15](n)
}
