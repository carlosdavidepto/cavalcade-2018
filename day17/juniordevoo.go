package day17

import "fmt"

// default implementation, for acceptance test
var FizzBuzz func(int) string = NewFizzBuzzer().FizzBuzz

type FizzBuzzer struct{}

func NewFizzBuzzer() *FizzBuzzer {
	return &FizzBuzzer{}
}

func (fb *FizzBuzzer) FizzBuzz(n int) string {
	if n%3 == 0 && n%5 == 0 {
		return "fizzbuzz"
	} else if n%3 == 0 {
		return "fizz"
	} else if n%5 == 0 {
		return "buzz"
	} else {
		return fmt.Sprint(n)
	}
}
