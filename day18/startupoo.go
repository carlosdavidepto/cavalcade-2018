package day18

import "fmt"

var FizzBuzz func(int) string = InitFizzBuzz()

func InitFizzBuzz() func(int) string {
	c := NewNumberConverter()

	c.AddRule(15, "fizzbuzz")
	c.AddRule(3, "fizz")
	c.AddRule(5, "buzz")

	c.SetDefaultRule(func(n int) string {
		return fmt.Sprint(n)
	})

	return c.ApplyRules
}

// Implementation of NumberConverter Engine

type rule struct {
	i int
	s string
}

type NumberConverter struct {
	rules       []rule
	defaultRule func(int) string
}

func NewNumberConverter() *NumberConverter {
	return &NumberConverter{}
}

func (c *NumberConverter) AddRule(i int, s string) {
	c.rules = append(c.rules, rule{i, s})
}

func (c *NumberConverter) SetDefaultRule(f func(int) string) {
	c.defaultRule = f
}

func (c *NumberConverter) ApplyRules(n int) string {
	for _, r := range c.rules {
		if n%r.i == 0 {
			return r.s
		}
	}

	return c.defaultRule(n)
}
