package day19

type BuzzerImpl struct{}

func (f *BuzzerImpl) Buzz(fb FBState) {
	if fb.Number()%5 == 0 {
		fb.SetState(fb.Number(), fb.String()+"buzz")
	}
}
