package day19

func NewFBState(n int) *FBStateImpl {
	return &FBStateImpl{n, ""}
}

func NewFormater() *FormaterImpl {
	return &FormaterImpl{}
}

func NewFizzer() *FizzerImpl {
	return &FizzerImpl{}
}

func NewBuzzer() *BuzzerImpl {
	return &BuzzerImpl{}
}

func NewFizzBuzzManager() *FizzBuzzManagerImpl {
	return &FizzBuzzManagerImpl{
		NewFizzer(),
		NewBuzzer(),
		NewFormater(),
	}
}
