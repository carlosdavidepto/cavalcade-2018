package day19

type FBStateImpl struct {
	i int
	s string
}

func (f *FBStateImpl) SetState(i int, s string) {
	f.i = i
	f.s = s
}

func (f *FBStateImpl) Number() int {
	return f.i
}

func (f *FBStateImpl) String() string {
	return f.s
}
