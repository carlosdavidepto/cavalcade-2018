package day19

type FizzerImpl struct{}

func (f *FizzerImpl) Fizz(fb FBState) {
	if fb.Number()%3 == 0 {
		fb.SetState(fb.Number(), fb.String()+"fizz")
	}
}
