package day19

import "fmt"

type FormaterImpl struct{}

func (_ *FormaterImpl) Format(f FBState) string {
	if len(f.String()) == 0 {
		return fmt.Sprint(f.Number())
	} else {
		return f.String()
	}
}
