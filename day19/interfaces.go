package day19

// adapter for test harness
var FizzBuzz func(int) string = NewFizzBuzzManager().Manage

type FBState interface {
	SetState(int, string)
	Number() int
	String() string
}

type Fizzer interface {
	Fizz(FBState)
}

type Buzzer interface {
	Buzz(FBState)
}

type Formater interface {
	Format(FBState) string
}
