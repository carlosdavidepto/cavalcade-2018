package day19

type FizzBuzzManagerImpl struct {
	f Fizzer
	b Buzzer
	m Formater
}

func (i *FizzBuzzManagerImpl) Manage(n int) string {
	s := NewFBState(n)
	i.f.Fizz(s)
	i.b.Buzz(s)
	return i.m.Format(s)
}
