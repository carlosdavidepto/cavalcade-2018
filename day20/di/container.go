package di

import (
	"gitlab.com/carlosdavidepto/cavalcade-2018/day20/di/factories"
	"gitlab.com/carlosdavidepto/cavalcade-2018/day20/managers"
)

type Container struct {
	fizzBuzzManager *managers.FizzBuzzManager
}

func NewContainer() *Container {
	return &Container{}
}

func (c *Container) FizzBuzzManager() *managers.FizzBuzzManager {
	if c.fizzBuzzManager == nil {
		c.fizzBuzzManager = factories.FizzBuzzManager()
	}

	return c.fizzBuzzManager
}
