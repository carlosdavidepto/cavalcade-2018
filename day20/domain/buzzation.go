package domain

type Buzzation struct{}

func (_ *Buzzation) Transform(n int, s string) (int, string) {
	return n, s + "buzz"
}
