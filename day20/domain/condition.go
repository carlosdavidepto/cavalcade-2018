package domain

type Condition interface {
	IsTrue() bool
}
