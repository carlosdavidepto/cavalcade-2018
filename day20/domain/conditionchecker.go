package domain

type ConditionChecker interface {
	CheckCondition() bool
}
