package domain

type ConditionCheckerImpl struct {
	condition Condition
}

func (c *ConditionCheckerImpl) CheckCondition() bool {
	return c.condition.IsTrue()
}
