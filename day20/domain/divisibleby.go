package domain

type DivisibleBy struct {
	dividend int
	divisor  int
}

func (d *DivisibleBy) IsTrue() bool {
	return d.dividend%d.divisor == 0
}

func NewDivisibleBy(dividend, divisor int) *DivisibleBy {
	return &DivisibleBy{dividend, divisor}
}
