package domain

type Fizzation struct{}

func (_ *Fizzation) Transform(n int, s string) (int, string) {
	return n, s + "fizz"
}
