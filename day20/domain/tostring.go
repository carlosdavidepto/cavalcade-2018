package domain

import "fmt"

type ToString struct{}

func (_ *ToString) Transform(n int, s string) (int, string) {
	return n, fmt.Sprint(n)
}
