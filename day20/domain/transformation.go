package domain

type Transformation interface {
	Transform(int, string) (int, string)
}
