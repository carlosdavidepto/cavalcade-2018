package domain

type TransformationMapper interface {
	AddTransformation(Condition, Transformation)
	ApplyAll() string
	DefaultTransformation(Transformation)
}
