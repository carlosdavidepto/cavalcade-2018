package domain

type TransformationMapperImpl struct {
	number          int
	transformations []struct {
		c Condition
		t Transformation
	}
	defaultTransformation Transformation
}

func NewTransformationMapperImpl(n int) *TransformationMapperImpl {
	return &TransformationMapperImpl{number: n}
}

func (m *TransformationMapperImpl) AddTransformation(c Condition, t Transformation) {
	m.transformations = append(m.transformations, struct {
		c Condition
		t Transformation
	}{c, t})
}

func (m *TransformationMapperImpl) ApplyAll() string {
	n, s := m.number, ""

	for _, x := range m.transformations {
		if x.c.IsTrue() {
			n, s = x.t.Transform(n, s)
		}
	}

	if len(s) == 0 {
		_, def := m.defaultTransformation.Transform(n, "")
		return def
	}

	return s
}

func (m *TransformationMapperImpl) DefaultTransformation(t Transformation) {
	m.defaultTransformation = t
}
