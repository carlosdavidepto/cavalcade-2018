package day20

import "gitlab.com/carlosdavidepto/cavalcade-2018/day20/di"

var FizzBuzz func(int) string

func init() {
	dic := di.NewContainer()
	FizzBuzz = dic.FizzBuzzManager().Execute
}
