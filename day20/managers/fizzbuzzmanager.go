package managers

import "gitlab.com/carlosdavidepto/cavalcade-2018/day20/domain"

type FizzBuzzManager struct{}

func (m *FizzBuzzManager) Execute(n int) string {
	tm := domain.NewTransformationMapperImpl(n)

	tm.AddTransformation(domain.NewDivisibleBy(n, 3), &domain.Fizzation{})
	tm.AddTransformation(domain.NewDivisibleBy(n, 5), &domain.Buzzation{})

	tm.DefaultTransformation(&domain.ToString{})

	return tm.ApplyAll()
}
