package day21

import "fmt"

var FizzBuzz func(n int) string = Apply(
	Rule{3, "fizz"},
	Rule{5, "buzz"},
)

// DSL implementation ---

func Apply(rules ...Rule) func(int) string {
	return func(n int) string {
		s := ""

		for _, r := range rules {
			if n%r.divisor == 0 {
				s = s + r.part
			}
		}

		if s == "" {
			return fmt.Sprint(n)
		}

		return s
	}
}

type Rule struct {
	divisor int
	part    string
}
