package day22

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"os"
)

const CONFIG_PATH_ENV_VAR = "DAY22_CONFIG_PATH"

// FizzBuzz needs to be lazily initialized due to the use of `os.Env`.
// See test harness.
// var FizzBuzz func(n int) string = MakeXMLRulesApplicator()

func MakeXMLRulesApplicator() func(int) string {

	path := os.Getenv(CONFIG_PATH_ENV_VAR)
	config := LoadConfig(LoadConfigFileBytes(path))
	rules := config.Rules

	return func(n int) string {
		s := ""

		for _, r := range rules {
			if n%r.Divisor == 0 {
				s = s + r.Part
			}
		}

		if s == "" {
			return fmt.Sprint(n)
		}

		return s
	}
}

func LoadConfig(configBytes []byte) Config {
	config := Config{}

	err := xml.Unmarshal(configBytes, &config)
	if err != nil {
		panic(err)
	}

	return config
}

func LoadConfigFileBytes(path string) []byte {
	configBytes, err := ioutil.ReadFile(path)
	if err != nil {
		panic(err)
	}

	return configBytes
}

type Config struct {
	XMLName xml.Name `xml:"config"`
	Rules   []Rule   `xml:"rules>rule"`
}

type Rule struct {
	Divisor int    `xml:"divisor,attr"`
	Part    string `xml:"part,attr"`
}
