package day23

import "fmt"

func FizzBuzz(n int) string {
	return map[bool]map[bool]string{
		true:  {true: "fizzbuzz", false: "fizz"},
		false: {true: "buzz", false: fmt.Sprint(n)},
	}[n%3 == 0][n%5 == 0]
}
