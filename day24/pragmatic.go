package day24

import "fmt"

func FizzBuzz(n int) string {
	if n%3 == 0 {
		if n%5 == 0 {
			return "fizzbuzz"
		} else {
			return "fizz"
		}
	} else {
		if n%5 == 0 {
			return "buzz"
		} else {
			return fmt.Sprint(n)
		}
	}
}
